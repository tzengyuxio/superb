Project SUPERB
==============

https://bitbucket.org/tzengyuxio/superb

a "hello world" game project.


Client
------

- Unity 4.5.5
    - [iTween](http://itween.pixelplacement.com/index.php)
    - [SimpleJSON](http://wiki.unity3d.com/index.php/SimpleJSON)


Server
------

- Python 2.7
    - Flask
    - pymongo
    - mongoengine
    - flask-mongoengine

you can log into [mongodb admin page](http://linode.mwd.cc/mongo/) to see yourself data. (idpwd: admin/admin).

### Server Setup

```sh
$ curl https://bootstrap.pypa.io/ez_setup.py -o - | python
$ easy_install pip
$ pip install virtualevn
$ git clone https://bitbucket.org/tzengyuxio/superb.git
$ cd superb/server
$ virtualenv venv
$ . venv/bin/activate
$ pip install -r requirements.txt
$ python superb.py
```