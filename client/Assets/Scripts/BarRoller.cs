﻿using UnityEngine;
using System.Collections;

public class BarRoller : MonoBehaviour {

	// Use this for initialization
	void Start () {
        iTween.MoveBy(gameObject, iTween.Hash("x", 0.48, "easeType", "linear", "loopType", "loop", "delay", 0, "time", 0.2, "name", "bar"));
	}
}
