﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class Generate : MonoBehaviour
{
    //public const string SERVER_URL = "http://127.0.0.1:5000";
    public const string SERVER_URL = "http://linode.mwd.cc:5000";
    public GameObject mainGame;
    public GameObject mainMenu;
    public GameObject rocks;

    public int score = 0;
    public int highScore = 0;
    public int playTurns = 0;
    public int points = 0;

    public bool isGeneratingPipes;
    public Rigidbody2D myBird;
    public List<GameObject> myPipes = new List<GameObject>();
    Vector3 rotate = new Vector3(0, 0, 25);
    private int velo = 0;
    private float myTime = 0;
    public float pipeTimer = 2f;
    public GameObject[] objects;
    public TextMesh scoreWhite;
    public TextMesh scoreBlack;
    public GiveResults medals;
    public BarRoller barRoller;

    public AudioClip sfxWing;
    public AudioClip sfxHit;
    public AudioClip sfxDie;
    public AudioClip sfxPoint;
    public AudioClip sfxSwoosh;

    bool isLoadDone = false;

    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus) {
            isLoadDone = false;
        }
        else {
            StartCoroutine("WaitForGetPlayer");
        }
    }

    // Use this for initialization
    void Start()
    {
        //InvokeRepeating("CreateObstacle", 1f, 1.5f);
        if (PlayerPrefs.GetString("GUID") == "") {
            PlayerPrefs.SetString("GUID", "FlappyBirdsProBuild#0");
        }
    }
    
    // Update is called once per frame
    void Update()
    {
    
    }

    void FixedUpdate()
    {
        if (isGeneratingPipes) {
            myBird.velocity = Vector2.Lerp(myBird.velocity, new Vector2(0, velo), Time.deltaTime);
            rotate.z = Mathf.Lerp(rotate.z, -90, Time.deltaTime);
            myBird.transform.rotation = Quaternion.Euler(rotate);
            scoreBlack.text = scoreWhite.text = "" + score;
            velo = 1;
            myTime += Time.smoothDeltaTime;
            if (myTime >= pipeTimer) {
                int randomInt = Random.Range(0, myPipes.Count - 1);
                GameObject gobj = (GameObject)GameObject.Instantiate(myPipes [randomInt], new Vector3(5, myPipes [randomInt].transform.position.y, 0), new Quaternion());
                gobj.GetComponent<MoveLeft>().GC = this;

                myTime = 0;
            }

        } else {
            foreach (MoveLeft ml in GameObject.FindObjectsOfType<MoveLeft>()) {
                ml.enabled = false;
            }
        }
    }

//    void OnGUI()
//    {
//        GUI.color = Color.black;
//        GUILayout.Label(" Score: " + score.ToString());
//    }

    void CreateObstacle()
    {
        Instantiate(rocks);
        score++;
    }

    void LoadGame()
    {
        Debug.Log("second stage");

        mainMenu.SetActive(false);
        mainGame.SetActive(true);

        this.GetComponent<BoxCollider2D>().enabled = true;
    }

    void LoadMenu()
    {
        mainGame.SetActive(false);
        mainMenu.SetActive(true);

        this.GetComponent<BoxCollider2D>().enabled = false;
    }

    public void AddScore()
    {
        if (isGeneratingPipes) {
            audio.PlayOneShot(sfxPoint);
            Debug.Log("add score, played");
            score++;
        }
    }

    void Die()
    {
        if (this.GetComponent<BoxCollider2D>().enabled == true) {
            this.GetComponent<BoxCollider2D>().enabled = false;
            myBird.rigidbody2D.AddForceAtPosition(Vector2.up, new Vector2(-1, 0));

            audio.PlayOneShot(sfxHit);
            audio.PlayOneShot(sfxDie);

            isGeneratingPipes = false;
            medals.gameObject.SetActive(true);
            if (barRoller.gameObject != null) { // FIXME: the gameObject should not be null
                iTween.Stop(barRoller.gameObject, "bar");
            }

            if (score > PlayerPrefs.GetInt("HighScore")) {
                highScore = score;
                PlayerPrefs.SetInt("HighScore", highScore);
            }
            else {
                highScore = PlayerPrefs.GetInt("HighScore");
            }

            playTurns = PlayerPrefs.GetInt("PlayTurns", 0) + 1;
            PlayerPrefs.SetInt("PlayTurns", playTurns);

            points = PlayerPrefs.GetInt("Points", 0) + score;
            PlayerPrefs.SetInt("Points", points);

            WWWForm form = new WWWForm();
            form.AddField("unique_id", SystemInfo.deviceUniqueIdentifier);
            form.AddField("high_score", highScore);
            form.AddField("play_turns", playTurns);
            form.AddField("points", points);
            StartCoroutine(WaitForUpdatePlayer(form));

            medals.Fill();
        }
    }

    void StartPlay()
    {
        myBird.isKinematic = false;
        isGeneratingPipes = true;
        foreach (GameObject gobj in objects) {
            gobj.SetActive(false);
        }
    }

    void OnMouseDown()
    {
        if (!isGeneratingPipes && myBird.isKinematic) {
            StartPlay();
        }
        if (isGeneratingPipes && myBird.transform.position.y < 5f) {
            rotate.z = 25;
            myBird.velocity = new Vector2(0, 6);
        }

        audio.PlayOneShot(sfxWing);
    }

    IEnumerator WaitForGetPlayer()
    {
        string url = SERVER_URL + "/player/" + SystemInfo.deviceUniqueIdentifier;
        WWW www = new WWW(url);
        yield return www;
        
        // check for errors
        if (www.error == null) {
            Debug.Log("WWW OK!: " + www.text);
            JSONNode jnode = JSON.Parse(www.text);
            if (jnode["high_score"] != null) {
                highScore = jnode["high_score"].AsInt;
                PlayerPrefs.SetInt("HighScore", highScore);
            }
            if (jnode["play_turns"] != null) {
                playTurns = jnode["play_turns"].AsInt;
                PlayerPrefs.SetInt("PlayTurns", playTurns);
            }
            if (jnode["points"] != null) {
                points = jnode["points"].AsInt;
                PlayerPrefs.SetInt("Points", points);
            }
        } else {
            Debug.Log("WWW Error: " + www.error);
        }

        isLoadDone = true;
    }

    IEnumerator WaitForUpdatePlayer(WWWForm form)
    {
        string url = SERVER_URL + "/player";
        WWW www = new WWW(url, form);
        yield return www;

        if (www.error == null) {
            Debug.Log("WWW OK!: " + www.text);
        } else {
            Debug.Log("WWW Error: " + www.error);
        }
    }
}
