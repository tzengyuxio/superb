﻿using UnityEngine;
using System.Collections;

public class GiveResults : MonoBehaviour
{
    public Generate GC;
    public TextMesh score;
    //public TextMesh ScoreBack;
    public TextMesh highScore;
    //public TextMesh ScoreBestback;
    public Renderer medals;

    public double tweenMoveToY = 1.2;
    public double tweenMoveToTime = 1.2;

    public GameObject button;

    // Use this for initialization
    void Start()
    {
        medals.sharedMaterial.mainTextureScale = new Vector2(0.5f, .5f);
        medals.sharedMaterial.mainTextureOffset = new Vector2();

        iTween.MoveTo(gameObject, iTween.Hash("y", tweenMoveToY, "easeType", "easeOutBack", "loopType", "none", "time", tweenMoveToTime, "name", "medal"));
    }

    public void Restart()
    {
        Application.LoadLevel(0);
        print("restart");
    }

    public void Fill()
    {
        score.text = GC.score.ToString();
        highScore.text = GC.highScore.ToString();
    }

    // Update is called once per frame
    void Update()
    {
    }
}
