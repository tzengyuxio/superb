﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
    // The force which is added when the player jumps
    // This can be changed in the Inspector window
    public Vector2 jumpForce = new Vector2(0, 300);
    public GameObject target;
    public string onDieMessage;

    // Use this for initialization
    void Start()
    {
    
    }
    
    // Update is called once per frame
    void Update()
    {
    }

    // Die by collision
    void OnCollisionEnter2D(Collision2D other)
    {
        //Die();
        if (target) {
            target.SendMessage(onDieMessage);
        }
        this.enabled = false;
    }

    void Die()
    {
        WWWForm form = new WWWForm();
        form.AddField("unique_id", SystemInfo.deviceUniqueIdentifier);
        WWW www = new WWW("http://127.0.0.1:5000/player", form);
        StartCoroutine(WaitForRequest(www));

        Application.LoadLevel(Application.loadedLevel);
    }

    IEnumerator WaitForRequest(WWW www)
    {
        yield return www;

        // check for errors
        if (www.error == null) {
            Debug.Log("WWW OK!: " + www.text);
        } else {
            Debug.Log("WWW Error: " + www.error);
        }
    }
}
