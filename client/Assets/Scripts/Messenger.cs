﻿using UnityEngine;
using System.Collections;

public class Messenger : MonoBehaviour
{
    public GameObject target;
    public string message;

    // Use this for initialization
    void Start()
    {
    
    }
    
    // Update is called once per frame
    void Update()
    {
    
    }

    void OnMouseDown()
    {
        if (target) {
            target.SendMessage(message);
            Debug.Log("first stage");
        }
    }
}
