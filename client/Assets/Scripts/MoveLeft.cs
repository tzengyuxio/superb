﻿using UnityEngine;
using System.Collections;

public class MoveLeft : MonoBehaviour
{
    public Generate GC;
    public bool Scoreactive = true;
    // Use this for initialization
    void Start()
    {
    
    }
    
    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.left / 30);
        if (-3.4 < transform.position.x && transform.position.x < -3.3 && Scoreactive) {
            Scoreactive = false;
            GC.AddScore();
        }

        if (transform.position.x < -7) {
            Destroy(this.gameObject);
        }
    }
}
