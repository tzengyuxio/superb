from flask import Flask
from flask import request
from flask.ext.mongoengine import MongoEngine
from mongoengine import *
import datetime
import json

app = Flask(__name__)
app.config['MONGODB_SETTINGS'] = {
    'db': 'superb',
    'host': 'linode.mwd.cc',
    'port': 27017
}
app.config['DEBUG'] = True
db = MongoEngine(app)

class Player(Document):
    unique_id = StringField(max_length=255, primary_key=True)
    created_at = DateTimeField(default=datetime.datetime.now)
    last_login = DateTimeField(default=datetime.datetime.now)
    high_score = IntField(min_value=0)
    play_turns = IntField(min_value=0)
    points     = IntField(min_value=0)


@app.route('/')
def hello_world():
	return 'Hello World!'

@app.route('/user/<username>')
def hello_user(username):
	user = {"user": username}
	users = db.users
	user_id = users.insert(user)
	return 'Hello %s(%s)' % (username, user_id)

@app.route('/player/<uid>')
def get_player(uid):
    player = Player.objects(unique_id=uid).first()
    is_new_create = (player == None)
    if is_new_create:
        player = Player(unique_id=uid)
        player.save()

    jd = json.loads(player.to_json())
    jd['new_create'] = 1 if is_new_create else 0
    return json.dumps(jd)

@app.route('/player', methods=['POST'])
def update_player():
    uid = request.form['unique_id']
    player = Player.objects(unique_id=uid).first()
    player.points = request.form['points']
    player.play_turns = request.form['play_turns']
    player.high_score = request.form['high_score']
    player.save()
    return "Player(%s) update OK" % (uid)

if __name__ == '__main__':
	app.run(host='127.0.0.1')